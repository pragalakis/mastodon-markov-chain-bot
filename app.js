require('dotenv').config();

const mastodon = require('mastodon');
const markov = require('./markov-chain.js');

try {
  const mstdn = new mastodon({
    access_token: process.env.MASTODON_ACCESS_TOKEN,
    api_url: process.env.API
  });

  const textFile = '/examples/asimov.txt';
  const numOfChars = 3;
  const numOfReps = 20;

  // generate markov chain text
  markov(textFile, numOfChars, numOfReps)
    .then(text =>
      // post text on mastodon
      mstdn.post('statuses', { status: text }, (error, data, response) => {
        if (error) {
          console.log(error);
        } else {
          console.log('toot toot');
          console.log(`posted status: ${text}`);
        }
      })
    )
    .catch(error => console.log(error));
} catch (error) {
  console.error(error);
}
