# [deprecated] mastodon-markov-chain-bot

Moved to codeberg: https://codeberg.org/evasync/mastodon-markov-chain-bot

A simple node.js bot - that generates markov chain text statuses and posts them on mastodon.
[Example Mastodon Account](https://botsin.space/@bot_nbdy)

## What's inside?

- dotenv - a module that loads environment variables from a .env file
- markov-chain.js - a markov chain generator
- mastodon - the Mastodon API client for node

## How to start?

- Clone this repo
- Remove the `.git` folder
- Add your mastodon access token and api url on .env file
- Add a text file (similar with the examples/asimov.txt) as a source to generate the markov chain sentences
- `npm start`

This will post a markov chain sentence on your mastodon account.

For automation: you can schedule a Cron Job to run the script.

## License

MIT
