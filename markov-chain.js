const fs = require('fs');

module.exports = (file, nc, n) => {
  return new Promise((resolve, reject) => {
    const data = fs.createReadStream(__dirname + file);
    let string = '';

    data.on('error', err => reject(err));
    data.on('data', d => {
      string += d.toString();
    });

    data.on('end', () => {
      // generate dictionary
      const d = markov(string);

      // generate text
      const t = generateText(d);

      return resolve(t);
    });

    function markov(text) {
      const lex = {};
      text = text.split('');

      for (let i = 0; i < text.length - nc; i++) {
        // the key and values length is equal to nc
        const key = text.slice(i, i + nc).join('');
        if (!lex[key]) lex[key] = [];

        const value = text.slice(i + nc, i + 2 * nc).join('');
        lex[key].push(value);
      }

      return lex;
    }

    function generateText(lex) {
      const arr = [];

      // random key
      let key = Object.keys(lex)[
        Math.floor(Math.random() * Object.keys(lex).length)
      ];
      arr.push(key);

      while (n > 0) {
        // random value with the previous key
        const value = lex[key][Math.floor(Math.random() * lex[key].length)];
        arr.push(value);

        // current key
        key = value;
        n--;
      }

      return arr.join('');
    }
  });
};
